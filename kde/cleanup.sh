#!/bin/bash
sudo pacman -Rs feh
sudo pacman -Rs plasma
rm -r ~/.config/autostart-scripts
rm -r ~/.config/plasma-workspace/env
echo "KDE cleanup complete."
