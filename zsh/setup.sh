#!/bin/bash

# One liner for getting the directory the script resides in.
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# Link all configuration files from the repository to the local filesystem.
echo "Setting up symbolic links."
sudo pacman -S thefuck zsh
rm ~/.zshrc
ln -s "$SCRIPT_DIR/.zshrc" ~/.zshrc

echo "Setting up repositories."
git clone https://github.com/robbyrussell/oh-my-zsh ~/.oh-my-zsh
git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
git clone https://github.com/bhilburn/powerlevel9k ~/.oh-my-zsh/custom/themes/powerlevel9k

echo "Installing fonts."
curl -Lo SauceCodeProNerdFontCompleteMono.ttf \
  https://github.com/ryanoasis/nerd-fonts/blob/master/patched-fonts/SourceCodePro/Regular/complete/Sauce%20Code%20Pro%20Nerd%20Font%20Complete%20Mono.ttf
kfontview SauceCodeProNerdFontCompleteMono.ttf

echo "Done! Please remember to reload your terminal."
