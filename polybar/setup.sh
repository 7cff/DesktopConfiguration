#!/bin/bash
# One liner for getting the directory the script resides in.
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

rm -r ~/.config/polybar
ln -s $SCRIPT_DIR/polybar ~/.config/polybar
